import React, { Component } from 'react';
import {CSSTransition} from 'react-transition-group';
import Header from './components/Header';
import Footer from './components/Footer';
class Notfound extends Component {
    constructor(props){
      super(props);
      this.state = {
          cartNumber: 0
      };
                  
  }
  render() {
    return (
      <CSSTransition in={true} appear={true} timeout={1000} classNames="fade">
      <div className="l-notFound">
        <Header cart={this.state.cartNumber}/>
        <h1>404</h1><br/>
        <h2>Página Não Encontrada</h2>
        <Footer/>
      </div>
      </CSSTransition>
    );
  }
};

export default Notfound;
