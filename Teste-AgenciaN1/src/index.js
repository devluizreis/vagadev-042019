import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import App from './App';
import ConstructionPage from './ConstructionPage';
import Notfound from './Notfound';
import './styles/index.css'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
        <BrowserRouter>
            <div>
                <Switch>
                <Route exact path="/" component={App}/>
                <Route exact path="/presentes" component={ConstructionPage}/>
                <Route exact path="/games" component={ConstructionPage}/>
                <Route component={Notfound} />
                </Switch>
            </div>
        </BrowserRouter>
        ),
        document.getElementById('root')
        );
registerServiceWorker();
