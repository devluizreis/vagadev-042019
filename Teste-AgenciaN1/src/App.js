import React, { Component } from 'react';
import {CSSTransition} from 'react-transition-group';
import Header from './components/Header';
import Product from './components/Product';
import Footer from './components/Footer';
import Modal from './components/Modal';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
        showModal: false,
        cartNumber: 0
    };
                
}

showModal = () => {
  this.setState({showModal: !this.state.showModal});  
};

hideModal = () => {
  this.setState({showModal: !this.state.showModal});
  this.setState({cartNumber: this.state.cartNumber + 1});  
};


  render() {
    return (
      <CSSTransition in={true} appear={true} timeout={1000} classNames="fade">
      <div>
          <Header cart={this.state.cartNumber} />
          {this.state.showModal &&(<Modal handleClick={this.hideModal} />)}
          <div>
          <Product handleClick={this.showModal}/>
          </div>
          <Footer/>
      </div>
      </CSSTransition>
    );
  }
};

export default App;
