import React, { Component } from 'react';
import {CSSTransition} from 'react-transition-group';
import constructionPage from './Img/construction-page.jpeg';
import Header from './components/Header';
import Footer from './components/Footer';


class ConstructionPage extends Component {
  constructor(props){
    super(props);
    this.state = {
        cartNumber: 0
    };
                
}
  render() {
    return (
      <CSSTransition in={true} appear={true} timeout={1000} classNames="fade">
      <div className="l-constructionPage">
        <Header cart={this.state.cartNumber}/>
        <div className="img-constructionPage">
        <img src={constructionPage} alt="Imagem de página em construção"/>
        </div>
        <Footer />
      </div>
      </CSSTransition>
    );
  }
};

export default ConstructionPage;
