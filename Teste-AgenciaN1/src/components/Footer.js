import React, { Component } from 'react';
import logo from '../Img/Agencia-n1-light';

class Footer extends Component {
    
  render() {
      
    return (
      <div className="l-footer">
        <footer>
            <div className="darkBlue">
        <img src={logo} align="middle" alt="Logo da Agência N1 no rodapé"/>
        </div>
        <div className="lightBlue">
        <p>Agência N1 - Todos os direitos reservados</p>
        </div>
        </footer>
      </div>
    );
  }
};

export default Footer;
