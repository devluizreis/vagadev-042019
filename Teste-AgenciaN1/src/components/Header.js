import React, { Component } from 'react';
import AutoComplete from './AutoComplete';
import AutoCompleteMobile from './AutoComplete';
import logo from '../Img/Agencia-n1';
import cart from '../Img/shopping-cart.svg';
import { FaSearch } from "react-icons/fa";
import { TiWavesOutline } from "react-icons/ti";
import { TiTimesOutline } from "react-icons/ti";

class Header extends Component {
    
  constructor(props, context) {
    super(props, context);

    this.state = {
        active: false,
        changeIcon: false
    };

    this.handleClick = this.handleClick.bind(this);
};

handleClick() {
    this.setState({
        active: !this.state.active,
        changeIcon: !this.state.changeIcon
    });
};
    
  render() {
      
    return (
      <div className="l-header">
        <header>
          <div className="logo">
        <a href="/"><img src={logo} align="middle" alt="Logo da Agência N1"/></a>
        </div>
        <ul>
          <li><a href="/games">Games</a></li>
          <li><a href="/presentes">Presentes</a></li>
          <li><a className="sale" href="/">Sale</a></li>
        </ul>
        <form className="form-search">
        <button><FaSearch className="search-icon"/></button><AutoComplete/>
        </form>
        <div className="cart">
        <img className="cart__img" src={cart} alt="icone de carrinho de compras"/>
        <div className="cart__number">{this.props.cart}</div>
        </div>
        {/* <div className="cart__itens">
        <div className="cart__itens___header">
        <span>Minhas compras</span><small>Quantidade: {this.props.cart}</small>
        </div>
        <div className="cart__itens___body">
        <span>Valor Total:</span>
        </div>
        </div> */}
        {!this.state.active &&(<TiWavesOutline className="menu" onClick={this.handleClick}/>)}
        {this.state.active &&(<TiTimesOutline className="menu" onClick={this.handleClick}/>)}
        {this.state.active &&(<div className="menu-links">
        <ul>
          <li><a href="/games">Games</a></li>
          <li><a href="/presentes">Presentes</a></li>
          <li><a className="sale" href="/">Sale</a></li>
        </ul>
        <form className="menu-search">
        <button><FaSearch className="search-icon"/></button><AutoCompleteMobile/>
        </form>
        </div>)}
        </header>
      </div>
    );
  }
};

export default Header;
