import React, { Component } from 'react';
import MarioBombeiro from '../Img/Mario-Bombeiro';
import DoctorStrange from '../Img/Doctor-Strange';
import SuperMarioBros from '../Img/Super-Mario-Bros';
import Ryu from '../Img/Ryu';
class Product extends Component {
  constructor(props){
    super(props);
    this.state = {
        fretePrefix: '',
        fretesufix: ''
    };

    this.Prefix = (e) =>{
      const regex = /^[0-9\b]+$/;
      if(e.target.value === '' || regex.test(e.target.value)){
      this.setState({fretePrefix: e.target.value});
      }
    }

    this.Sufix = (e) =>{
      const regex = /^[0-9\b]+$/;
      if(e.target.value === '' || regex.test(e.target.value)){
      this.setState({fretesufix: e.target.value});
      }
    }
}
  render() {
      
    return (
      <div className="l-product">
      <section className="buy">
      <div className="product__buy">
        <ul>
          <li><a>N1</a></li> &#x203A;
          <li><a>action figures</a></li> &#x203A;
          <li><a className="mario">Super Mario</a></li>
        </ul>
        <img className="small-box" src={MarioBombeiro} alt="imagem pequena de Mario Bombeiro"/>
        <img className="box"src={MarioBombeiro} alt="imagem de Mario Bombeiro"/>
        <div className="product__right">
        <h2>Action figure bombeiro Mario topzeira das galaxias</h2>
        <div className="product__value">
        <small>de R$189,90</small>
        <small>por <div>R$149,90</div></small>
        <button onClick={this.props.handleClick}>Compra ae</button>
        </div>
        <div className="product__frete">
        <p>Calcule o frete</p>
        <form>
        <input value={this.state.fretePrefix} onChange={this.Prefix} placeholder="00000" maxLength="5" required/><input value={this.state.fretesufix} onChange={this.Sufix} placeholder="000" maxLength="3" required/><button>Calcular</button>
        </form>
        </div>
        </div>
        </div>
        </section>
        
        <section className="desc">
        <div className="product__desc">
        <h3>Descrição do Produto</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lectus nibh, maximus
             sit amet convallis ut, malesuada sit amet leo. In feugiat, mauris sit amet rhoncus 
             vehicula, leo leo laoreet felis, eget feugiat urna urna ac libero. Pellentesque
              lacinia porttitor turpis, sit amet molestie velit consectetur quis. Vivamus
               nec neque sed purus gravida pulvinar non in tortor. Phasellus dolor enim,
                elementum a nisl vitae, consectetur bibendum nunc. Vestibulum libero quam,
                 dignissim id efficitur ut, scelerisque eu turpis. Etiam volutpat non ipsum 
                 vitae finibus. Suspendisse id viverra nulla, in imperdiet ex.</p>
        <p>Sed vel velit eget ligula tincidunt vehicula. Proin tempor sollicitudin pellentesque. 
            Vestibulum gravida dui nisl, sit amet feugiat quam hendrerit id.</p>
        </div>
        </section>

        <section className="others">
        <div className="product__others">
        <h3>Quem viu, viu também</h3>
        <div className="others">
        <img src={DoctorStrange} alt="Imagem de Doutor Estranho"/>
        <p>Action Figure Doctor Strange e Burning Flame Set - SH.Figuarts</p>
        <small>de R$725,90</small>
        <span>por R$624,90</span>
        </div>
        <div className="others">
        <img src={SuperMarioBros} alt="imagem de Super Mario Bros"/>
        <p>Action Figures - Super Mario Bros - Bandai</p>
        <small>de R$189,90</small>
        <span>por R$149,90</span>
        </div>
        <div className="others">
        <img src={Ryu} alt="Imagem de Ryu (street fight)"/>
        <p>Figura street Fighter Ryu</p>
        <small>de R$5,500,20</small>
        <span>por R$5.259,00</span>
        </div>
        </div>
        </section>
      </div>
    );
  }
};

export default Product;
