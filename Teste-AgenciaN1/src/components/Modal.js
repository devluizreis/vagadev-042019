import React, { Component } from 'react';
import {CSSTransition} from 'react-transition-group';
import { FaCheckCircle } from "react-icons/fa";

class Modal extends Component {

  render() {
    return (
      <CSSTransition in={true} appear={true} timeout={1000} classNames="fade">
        <div className="l-modal">
        <div>
            <FaCheckCircle className="check"/>
            <p>produto adicionado ao carrinho</p>
            <button onClick={this.props.handleClick}>OK!</button>
        </div>
      </div>
      </CSSTransition>
    );
  }
};

export default Modal;
