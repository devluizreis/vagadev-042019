import React, { Component } from 'react';

class AutoCompleteMobile extends Component {
    constructor(props) {
        super(props);
        this.items = [
            'Action Figure Doctor Strange e Burning Flame Set - SH.Figuarts',
            'Action Figures - Super Mario Bros - Bandai',
            'Figura street Fighter Ryu',
        ];
        this.state = {
            suggestions: [],
            text: ''
        };
    }

    onTextChange = (e) => {
        const value = e.target.value;
        let suggestions = [];
        if (value.length > 0) {
            const regex = new RegExp(`^${value}`, 'i');
            suggestions = this.items.sort().filter(v => regex.test(v));

        }
        this.setState(() => ({ suggestions, text: value }));
    }

    suggestionSelected(value){
        this.setState(() =>({
            text:value, 
            suggestion: []
        }));
    }

    renderSuggestions() {
        const { suggestions } = this.state;
        if (suggestions.length === 0) {
            return null;
        }
            return(
            <ul className="suggestion">
                {suggestions.map((item) => <li key={item.id} onClick={()=> this.suggestionSelected(item)}>{item}</li>)}
            </ul>
            );
        
    }

    render() {
        const{ text } = this.state;
        return (
            <div>
                <input value={text} onChange={this.onTextChange} type="text" placeholder="Digite o que procura" required/>
                {this.renderSuggestions()}
            </div>
        );
    }
}
export default AutoCompleteMobile;